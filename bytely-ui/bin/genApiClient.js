const request = require('request');
const CodeGen = require('swagger-js-codegen').CodeGen;
const fs = require('fs');
const path = require('path');

const args = process.argv;
let hostname;

console.debug(process.argv);

/**
 * argv[0] : program (i.e. 'node')
 * argv[1] : script name (i.e. 'script.js')
 * args[2] : program arguments
 */
if (args.length === 2) {
  hostname = "http://localhost:8080/v2/api-docs";
} else if (args.length === 3) {
  hostname = args[2];
} else {
  process.exit(1);
}

console.debug("Using hostname: " + hostname);

const classname = "BytelyServer";
let uiProjectRoot = path.dirname(__dirname).split(path.sep).pop();
let genClientApiLocation =
    path.resolve(uiProjectRoot, 'src', (classname + ".js"));
console.log(genClientApiLocation);

request(hostname, { json: true }, (err, res, body) => {
  if (err) {
    return console.error(err);
  }
  console.debug(JSON.stringify(body));

  const swagger = JSON.parse(JSON.stringify(body));
  const reactjsSourceCode = CodeGen.getReactCode(
      { className: 'BytelyServerApi', swagger: swagger });

  console.debug(reactjsSourceCode);

  fs.writeFile(genClientApiLocation, reactjsSourceCode, function(err) {
    if(err) {
      return console.log(err);
    }

    console.log("The file was saved!");
  });
});