import React, { Component } from 'react';
import { Form, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';

export default class RedirectUrlForm extends Component {
  constructor(props) {
    super(props);

    this.server = props.server;
    this.state = {
      response: '',
      redirectTo: 'redirect to',
      shorthand: 'shorthand'
    };

    this.handRedirectToChange = this.handRedirectToChange.bind(this);
    this.handShortHandChange = this.handShortHandChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handRedirectToChange(event) {
    this.setState({redirectTo: event.target.value});
  }


  handShortHandChange(event) {
    this.setState({shorthand: event.target.value});
  }


  handleSubmit(event) {
    var params = {
      redirectUrl : {
        redirectTo: this.state.redirectTo,
        shortHand:  this.state.shorthand
      }
    };

    this.server.saveUrlUsingPOST(params).then((value) => {
      console.log(value);
      this.setState({response: JSON.stringify(value)})
    }, (error) => {
      console.log(error);
      this.setState({response: error.message});
    });

    event.preventDefault();
  }

  render() {
    return (
        <form onSubmit={this.handleSubmit} className={"center-block"}>
          <ControlLabel>
            Shorthand:</ControlLabel>
          <input type="text" value={this.state.shorthand} onChange={this.handShortHandChange}/>

          <label>
            RedirectTo:
            <input type="text" value={this.state.redirectTo} onChange={this.handRedirectToChange}/>
          </label>
          <input type="submit" value="Submit" />
          <p>
            {this.state.response}
          </p>
        </form>
    );
  }
}


/**




 **/