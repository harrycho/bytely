import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

import './App.css';
import RedirectUrlForm from './RedirectUrlForm'
import BytelyServerApi from './BytelyServer'
import ClientProperties from './ClientProperties'

class App extends Component {
  constructor(props) {
    super(props);
    this.server = new BytelyServerApi(ClientProperties.server)
  }

  render() {
    return (
        <Grid>
          <Row>
            <Col>
              <RedirectUrlForm server={this.server}/>
            </Col>
          </Row>
        </Grid>
    );
  }
}

export default App;