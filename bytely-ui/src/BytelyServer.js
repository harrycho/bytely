/*jshint esversion: 6 */
/*global fetch, btoa */
import Q from 'q';

export default class BytelyServerApi {
    constructor(options) {
        let domain = (typeof options === 'object') ? options.domain : options;
        this.domain = domain ? domain : '';
        if (this.domain.length === 0) {
            throw new Error('Domain parameter must be specified as a string.');
        }
    }

    serializeQueryParams(parameters) {
      let str = [];
      for (let p in parameters) {
        if (parameters.hasOwnProperty(p)) {
          str.push(encodeURIComponent(p) + '=' + encodeURIComponent(
              parameters[p]));
        }
        return str.join('&');
      }
    }

    mergeQueryParams(parameters, queryParameters) {
        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    let parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }
        return queryParameters;
    }

    /**
     * HTTP Request
     * @method
     * @name BytelyServerApi#request
     * @param {string} method - http method
     * @param {string} url - url to do request
     * @param {object} parameters
     * @param {object} body - body parameters / object
     * @param {object} headers - header parameters
     * @param {object} queryParameters - querystring parameters
     * @param {object} form - form data object
     * @param {object} deferred - promise object
     */
    request (method, url, parameters, body, headers, queryParameters, form, deferred) {
        const queryParams = queryParameters && Object.keys(queryParameters).length ? this.serializeQueryParams(queryParameters) : null;
        const urlWithParams = url + (queryParams ? '?' + queryParams : '');

        if (body && !Object.keys(body).length) {
            body = undefined;
        }

        fetch(urlWithParams, {
            method,
            headers,
            body: JSON.stringify(body)
        }).then((response) => {
            return response.text();
        }).then((body) => {
            deferred.resolve(body);
        }).catch((error) => {
            deferred.reject(error);
        });
    };

    /**
     * saveUrl
     * @method
     * @name BytelyServerApi#saveUrlUsingPOST
     * @param {object} parameters - method options and parameters
     * @param {} parameters.redirectUrl - redirectUrl
     */
    saveUrlUsingPOST (parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/redirect';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers['Accept'] = ['*/*'];
        headers['Content-Type'] = ['application/json'];

        if (parameters['redirectUrl'] !== undefined) {
            body = parameters['redirectUrl'];
        }

        if (parameters['redirectUrl'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: redirectUrl'));
            return deferred.promise;
        }

        queryParameters = this.mergeQueryParams(parameters, queryParameters);

        this.request('POST', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };
    /**
     * Redirects given value to mapped location.
     * @method
     * @name BytelyServerApi#getUrlUsingGET
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.shorthand - shorthand
     */
    getUrlUsingGET (parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/redirect/{shorthand}';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers['Accept'] = ['*/*'];

        path = path.replace('{shorthand}', parameters['shorthand']);

        if (parameters['shorthand'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: shorthand'));
            return deferred.promise;
        }

        queryParameters = this.mergeQueryParams(parameters, queryParameters);

        this.request('GET', domain + path, parameters, body, headers, queryParameters, form, deferred);

        return deferred.promise;
    };

}