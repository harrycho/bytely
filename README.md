
## Resources
* [OpenAPI Specification v3.0.2](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md)
* [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [Swagger UI Tutorial (springframework.guru)](https://springframework.guru/spring-boot-restful-api-documentation-with-swagger-2/)
   * [Do **not** extend `WebMvcConfigurationSupport`](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-spring-mvc-auto-configuration)
* [Swagger Annotation API v1.5.0](http://docs.swagger.io/swagger-core/v1.5.0/apidocs/io/swagger/annotations/package-summary.html)
* [Managing Node (nvm)](https://stackoverflow.com/questions/8191459/how-do-i-update-node-js)
* [React Bootstrap Documentation](https://react-bootstrap.github.io/getting-started/introduction)

SwaggerUI accessible at <home>/swagger-ui.html. 