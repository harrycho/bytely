package me.attacker.bytely.dao;

import java.util.UUID;
import me.attacker.bytely.model.RedirectUrl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RedirectUrlRepository extends JpaRepository<RedirectUrl, UUID> {

  RedirectUrl findByShortHand(String shortHand);
}
