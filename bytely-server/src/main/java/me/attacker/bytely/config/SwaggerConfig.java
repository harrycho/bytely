package me.attacker.bytely.config;

import java.util.ArrayList;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {

  private ApiInfo apiInfo() {
    ApiInfoBuilder builder = new ApiInfoBuilder();
    return builder
        .title("Bytely REST API")
        .description("REST API for Bytely service.")
        .version("1.0.0")
        .termsOfServiceUrl("urn:tos")
        .contact(ApiInfo.DEFAULT_CONTACT)
        .license("Copyright (c) 2018 Harry Cho")
        .licenseUrl("404.com")
        .extensions(new ArrayList<>())
        .build();
  }

  @Bean
  public Docket productApi() {
    return new Docket(DocumentationType.SWAGGER_2)
        .useDefaultResponseMessages(false)
        .select()
        .apis(RequestHandlerSelectors.basePackage("me.attacker.bytely.controller.api"))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(apiInfo());

  }

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    /*
      Note on Swagger documentation
      "swagger-ui.html is the name of the swagger-ui page. While it cannot be changed one can
       configure the application such that landing on a particular URL re-directs the browser
       to the real swagger-ui location."
     */
    registry.addRedirectViewController(
        "/api/doc", "/api/doc.html");
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("swagger-ui.html")
        .addResourceLocations("classpath:/META-INF/resources/");

    registry.addResourceHandler("/webjars/**")
        .addResourceLocations("classpath:/META-INF/resources/webjars/");
  }
}