package me.attacker.bytely.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ShorthandValidator.class)
public @interface CheckShorthand {

    String message() default "{org.hibernate.validator.referenceguide.chapter06.CheckCase.message}";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };

}
