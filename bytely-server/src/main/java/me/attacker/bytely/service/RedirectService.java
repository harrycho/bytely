package me.attacker.bytely.service;

import java.util.Optional;
import me.attacker.bytely.common.RandomGenerator;
import me.attacker.bytely.dao.RedirectUrlRepository;
import me.attacker.bytely.model.RedirectUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RedirectService {

  private RedirectUrlRepository redirectUrlRepository;

  // TODO: Conflict should be rare.
  private final int GENERATE_RETRY_COUNT = 5;

  @Autowired
  public RedirectService(RedirectUrlRepository redirectUrlRepository) {
    this.redirectUrlRepository = redirectUrlRepository;
  }

  public Optional<String> get(String shortHand) {
    RedirectUrl redirectUrl = redirectUrlRepository.findByShortHand(shortHand);
    if (redirectUrl == null) {
      return Optional.empty();
    } else {
      if (redirectUrl.startsWithProtocol()) {
        return Optional.of(redirectUrl.getRedirectTo());
      } else {
        return Optional.of("http://" + redirectUrl.getRedirectTo());
      }
    }
  }

  public Optional<RedirectUrl> createIfNotExist(RedirectUrl redirectUrl) {
    String shorthand = redirectUrl.getShortHand();
    if (redirectUrl.getShortHand() == null) {
      for(int i = 0; i < this.GENERATE_RETRY_COUNT; i++) {
        shorthand = RandomGenerator.generateShorthand();
        if (this.get(shorthand).isPresent()) {
          shorthand = null;
          continue;
        }
        break;
      }
      if (shorthand == null) {
        // TODO: Do a better check of availability
        throw new RuntimeException("Unable to create due to random generating conflict.");
      }
    } else if (this.get(redirectUrl.getShortHand()).isPresent()) {
      return Optional.empty(); // Conflict
    }

    RedirectUrl toSave = new RedirectUrl(shorthand, redirectUrl.getRedirectTo());
    redirectUrlRepository.save(toSave);
    return Optional.of(toSave);

  }

}
