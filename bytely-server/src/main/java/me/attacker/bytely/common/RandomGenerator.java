package me.attacker.bytely.common;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomGenerator {

  private static final String ALPHABETS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  private static final String NUMBERS = "0123456789";
  private static final String ALPHANUMERIC = ALPHABETS + NUMBERS;
  private static final int DEFAULT_SHORTHAND_SIZE = 6;

  public static String generateShorthand() {
    return RandomStringUtils.random(1, ALPHABETS) +
        RandomStringUtils.random(DEFAULT_SHORTHAND_SIZE, ALPHANUMERIC);
  }

}
