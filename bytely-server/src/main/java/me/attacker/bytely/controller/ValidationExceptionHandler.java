package me.attacker.bytely.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.xml.bind.ValidationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * Request body are validated using @Valid annotation and constraints are annotated within the model.
 * Response parameters are validated using @Validated annotation at the controller level
 *
 */
@ControllerAdvice
public class ValidationExceptionHandler {

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<String> handleInvalidRequestBody(HttpServletRequest req, Exception e) {
    return ResponseEntity.badRequest().body(e.toString());
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<String> handleInvalidPathVariables(HttpServletRequest req,  Exception e) {
    return ResponseEntity.badRequest().body(e.toString());
  }
}
