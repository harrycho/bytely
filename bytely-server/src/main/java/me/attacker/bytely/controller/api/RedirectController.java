package me.attacker.bytely.controller.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import java.util.Optional;
import me.attacker.bytely.model.RedirectUrl;
import me.attacker.bytely.service.RedirectService;
import me.attacker.bytely.validation.CheckShorthand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/redirect")
@Api(value = "redirect", description = "redirects links.")
@Validated
public class RedirectController {

  private static final String SHORTHAND_VAL = "shorthand";

  private RedirectService redirectService;

  @Autowired
  public RedirectController(RedirectService redirectService) {
    this.redirectService = redirectService;
  }

  /**
   * Redirects request with 301 status code (Moved Permanently) with HTTP header's location set to
   * the new URL so that browser can handle the redirect.
   *
   * @param shorthand Shorthand version of the URL.
   * @return Status codes 301 if successful, 400 if shorthand is malformed or 404 if not found.
   */
  @ResponseStatus(code = HttpStatus.MOVED_PERMANENTLY)
  @RequestMapping(method = RequestMethod.GET, value = "{" + SHORTHAND_VAL + "}")
  @ApiOperation(value = "Redirects given value to mapped location.")
  @ApiResponses(value = {
      @ApiResponse(code = 301, message = "Successfully retrieved redirect URL."),
      @ApiResponse(code = 400, message = "Malformed request."),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found.")
  })
  public ResponseEntity<Void> getUrl(@PathVariable(SHORTHAND_VAL) @CheckShorthand String shorthand) {
    Optional<String> redirectUrl = redirectService.get(shorthand);
    if (redirectUrl.isPresent()) {
      HttpHeaders headers = new HttpHeaders();
      headers.setLocation(URI.create(redirectUrl.get()));
      return new ResponseEntity<>(headers, HttpStatus.MOVED_PERMANENTLY);
    } else {
      return ResponseEntity.notFound().build();
    }
  }

  @ResponseStatus(code = HttpStatus.OK)
  @RequestMapping(method = RequestMethod.POST)
  @ApiOperation(value = "")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successful save.", response = RedirectUrl.class),
      @ApiResponse(code = 400, message = "Malformed request."),
      @ApiResponse(code = 409, message = "Shorthand already exists.")
  })
  public ResponseEntity<RedirectUrl> saveUrl(@RequestBody @Valid RedirectUrl redirectUrl) {
    Optional<RedirectUrl> optionalRedirectUrl = redirectService.createIfNotExist(redirectUrl);
    return optionalRedirectUrl.map(ResponseEntity::ok)
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.CONFLICT));
  }

}
