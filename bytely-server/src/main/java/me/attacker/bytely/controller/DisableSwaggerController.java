package me.attacker.bytely.controller;

import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Profile("production")
@Controller
public class DisableSwaggerController {

  @RequestMapping(value = "swagger-ui.html", method = RequestMethod.GET)
  public ResponseEntity getSwagger() {
    return ResponseEntity.notFound().build();
  }
}
