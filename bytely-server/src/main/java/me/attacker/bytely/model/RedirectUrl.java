package me.attacker.bytely.model;

import com.google.common.base.Objects;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "redirect_url")
@ApiModel(value = "Redirect URL", description = "Represents from and to URL.")
public class RedirectUrl implements Serializable {

  public static final String SHORT_HAND_REGEX = "^[A-Za-z_][A-Za-z\\d]*$";

  private static final long serialVersionUID = -8258147997908983915L;

  @ApiModelProperty(readOnly = true)
  @Id
  @GeneratedValue(generator = "custom-uuid")
  @GenericGenerator(
      name = "custom-uuid",
      strategy = "org.hibernate.id.UUIDGenerator",
      parameters = {
          @Parameter(
              name = "uuid_gen_strategy_class",
              value = "org.hibernate.id.uuid.StandardRandomStrategy"
          )
      }
  )
  private UUID id;

  @Pattern(regexp = RedirectUrl.SHORT_HAND_REGEX)
  @ApiModelProperty(
      name = "shorthand",
      notes = "(Optional) Specified shorthand if supplied. Otherwise, "
          + "returns randomly generated value")
  @Column(unique = true)
  private String shortHand;

  @Pattern(regexp = "^((https?):\\/\\/)?(www.)?[a-zA-Z0-9]+\\.[a-zA-Z]+(\\/[a-zA-Z0-9#]+\\/?)*$")
  @NotNull
  @ApiModelProperty(
      name = "redirectTo",
      notes = "Must be a valid URL. http:// protocol is assumed if not specified",
      required = true)
  @Column(nullable = false)
  private String redirectTo;

  public RedirectUrl() {
  }

  public RedirectUrl(String shortHand, String redirectTo) {
    this.shortHand = shortHand;
    this.redirectTo = redirectTo;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getRedirectTo() {
    return redirectTo;
  }

  public void setRedirectTo(String redirectTo) {
    this.redirectTo = redirectTo;
  }

  public String getShortHand() {
    return shortHand;
  }

  public void setShortHand(String shortHand) {
    this.shortHand = shortHand;
  }

  public boolean startsWithProtocol() {
    return getRedirectTo().startsWith("http") || getRedirectTo().startsWith("https");
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RedirectUrl that = (RedirectUrl) o;
    return Objects.equal(id, that.id) &&
        Objects.equal(shortHand, that.shortHand) &&
        Objects.equal(redirectTo, that.redirectTo);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id, shortHand, redirectTo);
  }
}
