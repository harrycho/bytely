package me.attacker.bytely.unit.model;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import me.attacker.bytely.commons.RedirectUrlFactory;
import me.attacker.bytely.model.RedirectUrl;
import org.junit.BeforeClass;
import org.junit.Test;

public class RedirectUrlTest {

  private static Validator validator;

  @BeforeClass
  public static void setUpValidator() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  public void testUrlValidation() {
    RedirectUrl redirectUrl = RedirectUrlFactory.buildRandomRedirectUrl();
    redirectUrl.setShortHand("$$$$$");
    redirectUrl.setRedirectTo("def");
    Set<ConstraintViolation<RedirectUrl>> violations = validator.validate(redirectUrl);
    assertThat(violations.size(), is(2));
  }

  @Test
  public void testUrlNullableShortHand() {
    RedirectUrl redirectUrl = RedirectUrlFactory.buildRandomRedirectUrl();
    redirectUrl.setShortHand(null);
    Set<ConstraintViolation<RedirectUrl>> violations = validator.validate(redirectUrl);
    assertThat(violations.size(), is(0));
  }
}
