package me.attacker.bytely.integration;

import static me.attacker.bytely.commons.RedirectUrlFactory.buildRandomRedirectUrl;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

import me.attacker.bytely.dao.RedirectUrlRepository;
import me.attacker.bytely.model.RedirectUrl;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@DataJpaTest
public class RepositoryTest {

  @Autowired
  private RedirectUrlRepository redirectUrlRepository;

  @Test
  public void testTwoSavesResultInTwoDifferentIds() {
    RedirectUrl url1 = buildRandomRedirectUrl();
    redirectUrlRepository.save(url1);
    RedirectUrl url2 = buildRandomRedirectUrl();
    redirectUrlRepository.save(url2);
    assertThat(url1.getId(), not(equalTo(url2.getId())));
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void testSaveDuplicateShorthand() {
    RedirectUrl url1 = buildRandomRedirectUrl();
    redirectUrlRepository.saveAndFlush(url1);
    RedirectUrl url2 = buildRandomRedirectUrl();
    url2.setShortHand(url1.getShortHand());
    redirectUrlRepository.saveAndFlush(url2);
  }

  @Test
  public void testFindbyShorthand() {
    RedirectUrl urlSave = buildRandomRedirectUrl();
    redirectUrlRepository.save(urlSave);
    RedirectUrl urlGet = redirectUrlRepository.findByShortHand(urlSave.getShortHand());
    assertThat(urlSave, equalTo(urlGet));
  }

  @Test
  public void testGetNonExistentShorthand() {
    RedirectUrl urlGet = redirectUrlRepository.findByShortHand(RandomStringUtils.random(5));
    assertThat(urlGet, equalTo(null));
  }

}
