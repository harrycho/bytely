package me.attacker.bytely.integration;

import me.attacker.bytely.Main;
import me.attacker.bytely.dao.RedirectUrlRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = Main.class
)
@AutoConfigureMockMvc
@TestPropertySource(
    locations = "classpath:application-integrationtest.properties"
)
public class WebAppTest {

  @Autowired
  private MockMvc mvc;

  @Autowired
  private RedirectUrlRepository redirectUrlRepository;

  @Test
  public void someTest() throws Exception {
    mvc.perform(get("/hello"));
  }
}
