package me.attacker.bytely.commons;

import me.attacker.bytely.model.RedirectUrl;
import org.apache.commons.lang3.RandomStringUtils;

public class RedirectUrlFactory {

  public static RedirectUrl buildRandomRedirectUrl() {
    String shothand = RandomStringUtils.randomAlphabetic(1) +
        RandomStringUtils.randomAlphanumeric(29);
    String redirectTo = "http://" + RandomStringUtils.randomAlphanumeric(30) + ".com";
    System.out.println(redirectTo);
    return new RedirectUrl(shothand, redirectTo);
  }
}
